<?php

define('CONTROLLERS_DIR', __DIR__ . '/HW_21/controllers/');
define('VIEWS_DIR', __DIR__ . '/HW_21/views/');

require_once __DIR__ . '/HW_21/system/View.php';
require_once __DIR__ . '/HW_21/functions.php';
require_once __DIR__ . '/HW_21/enums/ValidationRules.php';
require_once __DIR__ . '/HW_21/traits/Validator.php';
require_once __DIR__ . '/HW_21/controllers/GreetingController.php';
require_once __DIR__ . '/HW_21/controllers/CalcController.php';
require_once __DIR__ . '/HW_21/system/Router.php';
require_once __DIR__ . '/HW_21/system/Request.php';

$router = new Router();
$router->addRouter('/greeting', [
  'get' => "GreetingController@greeting",
]);

$router->addRouter('/calc', [
  'get' => "CalcController@showForm",
  'post' => "CalcController@showResult"
]);

$router->processRouter(Request::getUrl(), Request::getMethod());