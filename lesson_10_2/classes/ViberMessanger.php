<?php

class ViberMessanger implements MessageInterface
{

  public function send(string $message): bool
  {
    echo 'send message via viber';
    return true;
  }

  public function receive(): string
  {
    return 'receive message via viber';
  }
}