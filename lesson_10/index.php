<?php
use classes\Blog;
use classes\News;
use classes\Post;

require_once __DIR__ . '/constants.php';
require_once CLASSES_DIR . 'Post.php';
require_once CLASSES_DIR . 'Blog.php';
require_once CLASSES_DIR . 'News.php';

$blog = new Blog('blog', 'blog content');
$news = new News('news', 'news content');
$post = new Post('post', 'news content');

function showPost(Post $post): void
{
  echo $post->getInfo();
}

showPost($blog);
showPost($news);
showPost($post);