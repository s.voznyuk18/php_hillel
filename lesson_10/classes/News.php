<?php

namespace classes;

class News extends Post
{
  public function getInfo(): string
  {
    $title = $this->getTitle();
    $content = $this->getContent();

    return "<i>$title</i><p>$content</p>";
  }
}