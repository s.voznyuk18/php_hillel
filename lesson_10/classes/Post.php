<?php

namespace classes;

class Post
{
  private string $title;
  private string $content;

  public function __construct(string $title, string $content)
  {
    $this->setTitle($title);
    $this->setContent($content);
  }

  public function setTitle(string $title): void
  {
    if(strlen($title) < 2) {
      throw new \Exception('invalid title');
    }

    $this->title = $title;
  }

  public function setContent(string $content): void
  {
    if(strlen($content) < 5) {
      throw new \Exception('invalid content');
    }

    $this->content = $content;
  }

  public function getTitle(): string
  {
    return $this->title;
  }

  public function getContent(): string
  {
    return $this->content;
  }

  public function getInfo(): string
  {
    $title = $this->getTitle();
    $content = $this->getContent();

    return "$title - $content <br>";
  }
}