<?php

$value = 'red';

$result = match($value) {
  'green' => 'green',
  'red' => 'red',
  'blue' => 'blue',
  'brown' => 'brown',
  'violet' => 'violet',
  'black' => 'black',
  default => 'white'
};

echo $result;
