CREATE TABLE IF NOT EXISTS `users`
(
    `id`          INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `first_name`  CHAR(100) NOT NULL,
    `second_name` CHAR(100) NOT NULL,
    `email`       CHAR(255) NOT NULL UNIQUE,
    `password`    CHAR(255) NOT NULL,
    `age`         TINYINT UNSIGNED,
    `gender`      ENUM ('male', 'female'),
    `created_at`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `deleted_at`  TIMESTAMP DEFAULT NULL
    ) ENGINE = InnoDB
    CHAR SET utf8;

CREATE TABLE IF NOT EXISTS `orders`
(
    `order_id`   INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `user_id`    INT UNSIGNED NOT NULL,
    `creates_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `deleted_at` TIMESTAMP DEFAULT NULL,
    FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ) ENGINE = InnoDB
    CHAR SET utf8;;

CREATE TABLE IF NOT EXISTS `products`
(
    `product_id`   INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `product_name` CHAR(255)              NOT NULL,
    `price`        DECIMAL(6, 2) UNSIGNED NOT NULL,
    `size`         ENUM ('S', 'M', 'L', 'XL'),
    `created_at`   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated-at`   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `deleted_at`   TIMESTAMP DEFAULT NULL
    ) ENGINE = InnoDB
    CHAR SET utf8;

CREATE TABLE IF NOT EXISTS `order_detail`
(
    `order_detail_id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `order_id`        INT UNSIGNED NOT NULL,
    `product_id`      INT UNSIGNED NOT NULL,
    `amount`          INT UNSIGNED NOT NULL,
    `created_at`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated-at`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `deleted_at`      TIMESTAMP DEFAULT NULL,
    FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`),
    FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`)
    ) ENGINE = InnoDB
    CHAR SET utf8;

INSERT INTO `products`(`product_name`, `size`, `price`)
VALUES ('cap', 'S', 10.00),
       ('cap', 'M', 12.00),
       ('pants', 'S', 25.99),
       ('t-shirt', 'S', 0);

INSERT INTO `users`(`first_name`, `second_name`, `email`, `password`, `age`, `gender`)
VALUES ('Serhii', 'Vozniuk', 'serhii@gmail.com', 'qwerty', 33, 'male');

INSERT INTO `users`(`first_name`, second_name, `email`, `password`, `age`, `gender`)
VALUES ('Ann', 'Ivanova', 'ann@gmail.com', 'qwerty12', 25, 'female');

INSERT INTO `users`(`first_name`, second_name, `email`, `password`, `age`, `gender`)
VALUES ('Olga', 'Chornenko', 'olga@gmail.com', 'qwerty12', 25, 'female');

INSERT INTO `orders`(`user_id`)
VALUES (1),
       (2),
       (1);

INSERT INTO `order_detail`(`order_id`, `product_id`, `amount`)
VALUES (1, 1, 1),
       (1, 3, 2),
       (1, 4, 1),
       (2, 4, 4);

SELECT `first_name`, `email` FROM `users`;
SELECT `first_name` FROM `users` WHERE `email` = 'serhii@gmail.com';

SELECT * FROM `users` WHERE `email` LIKE '%gmail.com';
SELECT * FROM `users` WHERE `age` BETWEEN 20 AND 30;
SELECT * FROM `users` WHERE  `age` NOT BETWEEN 20 AND 30;
SELECT * FROM `users` WHERE `age` > 30 AND `gender` LIKE 'male';
SELECT * FROM `products`;
SELECT * FROM `products` WHERE `price` > 15;
UPDATE `users` SET `age` = 35 WHERE `id` = 1;

# UPDATE `users` SET `gender` = 'male' WHERE `id` = 1;
# UPDATE `users` SET `age` = 25 WHERE `id` = 4;

# DELETE FROM `users` WHERE `id` = 3;

SELECT `first_name`, order_detail.amount, products.product_name, products.price
FROM `users`
         JOIN `orders` ON users.id = orders.order_id
         JOIN `order_detail` ON orders.order_id = order_detail.order_id
         JOIN `products` ON order_detail.product_id = products.product_id
WHERE id = 1;