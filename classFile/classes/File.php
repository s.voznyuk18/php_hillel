<?php
declare(strict_types=1);

class File
{
  private string $fileName;
  private string $type;
  private array $variantsType = ['action', 'error'];
  private string $message;
  private $file;

  function __construct(string $fileName)
  {
    $this->setName($fileName);
  }

  private function setName(string $fileName): void
  {
    $fileName = trim($fileName);

    if (empty($fileName)) {
      throw new Exception('$fileNme is required');
    }

    $this->fileName = $fileName;
  }

  private function setType(string $type): void
  {
    $type = trim($type);

    if (empty($type)) {
      throw new Exception('type is required');
    }
    if (!in_array($type, $this->variantsType)) {
      throw new Exception('the type should be action or error');
    }

    $this->type = $type;
  }

  private function setMessage(string $message): void
  {
    $message = trim($message);

    if (empty($message)) {
      throw new Exception('message is required');
    }
    if (strlen($message) < 5) {
      throw new Exception('message must be more than 5 characters ');
    }

    $date = date('d-m-Y-H-i-s');
    $message = "[$date][$this->type][$message]\n";
    $this->message = $message;
  }

  private function openFile(string $mode): void
  {
    $path = LOGS_DIR . $this->fileName;
    $this->file = fopen($path, $mode);
  }

  public function recordLog(string $message, string $type): void
  {
    $this->setType($type);
    $this->setMessage($message);
    $this->openFile('a+');
    fwrite($this->file, $this->message);
  }

  public function readLog(): Generator
  {
    $this->openFile('r');
    while (($line = fgets($this->file)) !== false) {
      yield $line;
    }
  }

  function __destruct()
  {
    if(isset($this->file)) {
      fclose($this->file);
    }
  }
}