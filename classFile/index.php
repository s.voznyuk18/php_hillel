<?php

require_once __DIR__ . '/constants.php';
require_once CLASSES_DIR . 'File.php';

try {
  $log = new File('log.txt');
  $log->recordLog('12345', 'action');

  $logs = $log->readLog();

  foreach ($logs as $log) {
    echo $log;
  }
} catch (Exception $exception){
  echo $exception ->getMessage() .PHP_EOL;
}

