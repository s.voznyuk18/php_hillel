<?php

class Worker
{
  private string $name;
  private string $position;

  public function __construct(string $name, string $position)
  {
    $this->setName($name);
    $this->setPosition($position);
  }

  public function setName(string $name): void
  {
    if (strlen($name) <= 2) {
      throw new Exception("name must be more than 2 characters");
    }
    $this->name = $name;
  }

  public function setPosition(string $position): void
  {
    if ($position !== 'manager' && $position !== 'developer' && $position !== 'tester') {
      throw new Exception('the position should be manager or developer or tester ');
    }
    $this->position = $position;
  }

  public function getWorker(): string
  {
    return "Name: $this->name - Position: $this->position";
  }
}