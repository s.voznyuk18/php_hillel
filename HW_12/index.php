<?php

require_once __DIR__ . '/classes/Worker.php';

try {
  $worker = new Worker('Serhii', 'manager');
  echo $worker->getWorker() . PHP_EOL;
} catch (Exception $exception) {
  echo $exception->getMessage() . PHP_EOL;
}