<?php

class Circle extends Figure
{
  private float $radius;
  private float $area;
  private float $perimeter;

  public function __construct(float $radius)
  {
    $this->setRadius($radius);
  }

  public function setRadius(float $radius): void
  {
    if ($radius <= 0) {
      throw new Exception('radius must be more 0');
    }

    $this->radius = $radius;
  }

  public function getRadius(): float
  {
    return $this->radius;
  }

  public function area(): void
  {
    $radius = $this->getRadius();

    $this->area = (pi() * $radius) ** 2;
  }

  public function perimeter(): void
  {
    $radius = $this->getRadius();

    $this->perimeter = 2 * pi() * $radius;
  }

  public function getArea(): float
  {
    $this->area();
    return $this->area;
  }

  public function getPerimeter(): float
  {
    $this->perimeter();
    return $this->perimeter;
  }

  public function getInfo(): string
  {
    $radius = $this->getRadius();
    $area = $this->getArea();
    $perimeter = $this->getPerimeter();
    return "
        <p>Circle</p>
        <i>radius:</i><i>$radius</i><br>
        <i>area :</i><i>$area </i><br>
        <i>perimeter:</i><i>$perimeter</i><br>
    ";
  }
}