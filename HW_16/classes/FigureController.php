<?php

class FigureController
{
  private Figure $figure;

  public function __construct(Figure $figure)
  {
    $this->figure = $figure;
  }

  public function getInfo(): void
  {
    try {
      echo $this->figure->getInfo();
    } catch (Exception $exception) {
      echo $exception->getMessage() . PHP_EOL;
    }
  }
}