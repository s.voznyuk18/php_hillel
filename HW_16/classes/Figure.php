<?php

abstract class Figure
{
  public abstract function area(): void;

  public abstract function perimeter(): void;

  public abstract function getInfo(): string;
}