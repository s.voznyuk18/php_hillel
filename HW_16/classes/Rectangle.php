<?php

class Rectangle extends Figure
{
  private string $width;
  private string $length;
  private float $area;
  private float $perimeter;

  public function __construct(float $width, float $length)
  {
    $this->setWidth($width);
    $this->setLength($length);
  }

  public function setWidth(float $width): void
  {
    if ($width <= 0) {
      throw new Exception('width must be more 0');
    }

    $this->width = $width;
  }

  public function setLength(float $length): void
  {
    if ($length <= 0) {
      throw new Exception('length must be more 0');
    }

    $this->length = $length;
  }

  public function getWidth(): float
  {
    return $this->width;
  }

  public function getLength(): float
  {
    return $this->length;
  }

  public function area(): void
  {
    $width = $this->getWidth();
    $length = $this->getLength();

    $this->area = $width * $length;
  }

  public function perimeter(): void
  {
    $width = $this->getWidth();
    $length = $this->getLength();

    $this->perimeter = ($width + $length) * 2;
  }

  public function getArea(): float
  {
    $this->area();
    return $this->area;
  }

  public function getPerimeter(): float
  {
    $this->perimeter();
    return $this->perimeter;
  }

  public function getInfo(): string
  {
    $width = $this->getWidth();
    $length = $this->getLength();
    $area = $this->getArea();
    $perimeter = $this->getPerimeter();
    return "
        <p>Rectangle</p>
        <i>width:</i><i>$width</i><br>
        <i>length:</i><i>$length</i><br>
        <i>area :</i><i>$area </i><br>
        <i>perimeter:</i><i>$perimeter</i><br>
    ";
  }
}