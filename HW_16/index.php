<?php
require_once __DIR__ . '/constants.php';
require_once CLASSES_DIR . '/Figure.php';
require_once CLASSES_DIR . '/Rectangle.php';
require_once CLASSES_DIR . '/Circle.php';
require_once CLASSES_DIR . '/FigureController.php';

$rectangle = new Rectangle(10, 10);
$circle = new Circle(10);

//$figureControlle = new FigureController($rectangle);
//$figureControlle->getInfo();

$figureControlle = new FigureController($circle);
$figureControlle->getInfo();