<?php

class Example
{
  private string $property;
  private float $value;

  public function __construct(string $property, float $value)
  {
    $this->setProperty($property);
    $this->setValue($value);
  }

  public function setProperty(string $property): void
  {
    if (!is_string($property)) {
      throw new Exception('Property 1 must be a string');
    }
    $this->property = $property;
  }

  public function setValue(float $value): void
  {
    if (!is_numeric($value)) {
      throw new Exception('value 2 must be numeric');
    }
    $this->value = $value;
  }

  public function getProperty(): string
  {
    return $this->property;
  }

  public function getValue(): string
  {
    return $this->value;
  }

  public function showProperties(): string
  {
    $value = $this->value;
    $property = $this->property;

    return "value: $value \nproperty: $property";
  }
}