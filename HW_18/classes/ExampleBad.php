<?php

class ExampleBad
{
  public $property='default_value';
  public $property2;

  public function __construct($property2)
  {
    $this->property2=$property2;
  }

  public function getProperty1()
  {
    if($this->property1==='default_value')
    {
      return "Property 1 has not been set.\n";
    }
    else
    {
      return $this->property1;
    }
  }

  public function setProperty1($value)
  {
    if(is_string($value))
    {
      $this->property1=$value;
    }
    else
    {
      echo "Invalid value. Property 1 must be a string.\n";
    }
  }

  public function getProperty2()
  {
    return $this->property2;
  }

  public function setProperty2($value)
  {
    if(is_numeric($value))
    {
      $this->property2=$value;
    }
    else
    {
      echo "Invalid value. Property 2 must be numeric.\n";
    }
  }

  public function displayProperties()
  {
    if($this->property1==='default_value'||!isset($this->property2))
    {
      echo "Properties have not been fully initialized.\n";
    }
    else
    {
      echo "Property 1: ".$this->property1."\n";
      echo "Property 2: ".$this->property2."\n";
    }
  }
}