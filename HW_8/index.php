<?php
  declare(strict_types=1);

  function makeRandArray(int $length, int $min = 0, int $max = 10): array 
  {
    $array = [];
    for($i = 0; $i < $length; $i++){
      $array[] = rand($min, $max);
    }
    return $array;
  }

  $randomArray = makeRandArray(10);

  function findMaxElem(array $arr): int 
  {
    $elem = 0;
    $length = count($arr);

    for($i = 0; $i < $length; $i++) {
      if($arr[$i] > $elem) $elem = $arr[$i];
    }

    return $elem;
  }

  function findMinElem(array $arr): int
  {
    $elem = $arr[0];
    $length = count($arr);

    for($i = 0; $i < $length; $i++) {
      if($arr[$i] < $elem) $elem = $arr[$i];
    }

    return $elem;
  }

  function sortArray(array $arr): array
  {
    $length = count($arr);

    for ($i = 0; $i < $length; $i++) {

      for ($j = 0; $j < $length - $i - 1; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
          $temp = $arr[$j];
          $arr[$j] = $arr[$j + 1];
          $arr[$j + 1] = $temp;
        }
      }
    }

    return $arr;
  }


  var_dump($randomArray);
  // var_dump(findMaxElem($randomArray ));
  // var_dump(findMinElem($randomArray ));

  var_dump(sortArray($randomArray ));



