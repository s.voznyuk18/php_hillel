<?php

class UpperText extends Text
{
  public function print(): string
  {
    return strtoupper($this->text);
  }
}