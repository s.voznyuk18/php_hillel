<?php

require_once __DIR__ . '/constants.php';
require_once CLASSES_DIR . 'Text.php';
require_once CLASSES_DIR . 'UpperText.php';

$text = new Text();
$upperText = new UpperText();

function showText(Text $text): void
{
  echo $text->print() . PHP_EOL;
}

showText($text);
showText($upperText);

