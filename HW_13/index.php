<?php
require_once __DIR__ . '/constants.php';
require_once CLASSES_DIR . 'TaskManager.php';
require_once ENUMS_DIR . '/TaskStatus.php';

$path = DB_DIR . 'tasks.json';

try {
  $taskManager = new TaskManager($path);

  $taskManager->addTask('task 2', 5);

  $taskManager->completeTask('65d0ee6bcdc63');

//  $sortedTaks = $taskManager->getTasks();
//  var_dump($sortedTaks);

//  $taskManager->deletetask('65d0ee57920c7');
} catch (Exception $exception) {
  echo $exception->getMessage() . PHP_EOL;
}
