<?php
declare(strict_types=1);

class TaskManager
{
  private string $path;
  private string $taskName;
  private int $priority;
  private string $status;
  private array $tasks;

  public function __construct(string $path)
  {
    $this->setPath($path);
    $this->getAllTasks($this->path);
  }

  private function setPath($path): void
  {
    $path = trim($path);

    if (!file_exists($path)) {
      throw new Exception('File not found');
    }

    $this->path = $path;
  }

  private function setTaskName(string $taskName): void
  {
    $taskName = trim($taskName);

    if (mb_strlen($taskName) < 5) {
      throw new Exception('$taskName should be more than 5 characters');
    }

    $this->taskName = $taskName;
  }

  private function setPriority(int $priority): void
  {
    if ($priority <= 0 || $priority > 10) {
      throw new Exception('priority should be from 1 to 10');
    }

    $this->priority = $priority;
  }

  private function setStatus(string $status): void
  {
    $allowedStatuses = TaskStatus::values();
    if (!in_array($status, $allowedStatuses)) {
      throw new Exception('status should be' . implode(',', $allowedStatuses));
    }

    $this->status = $status;
  }

  private function getAllTasks(string $path): void
  {
    $content = file_get_contents($path);
    $data = json_decode($content, true);

    if (!isset($data)) {
      $this->tasks = [];
    } else {
      $this->tasks = $data;
    }
  }

  private function saveTasks(array $tasks, string $path): void
  {
    $this->tasks = $tasks;
    $jsonData = json_encode($tasks);
    file_put_contents($path, $jsonData);
  }

  private function checkTaskById($tasks, $taskId): void
  {
    $idArray = array_column($tasks, 'id');

    if (!in_array($taskId, $idArray)) {
      throw new Exception("task $taskId not found");
    }
  }

  public function addTask(string $taskName, int $priority): void
  {
    $this->setTaskName($taskName);
    $this->setPriority($priority);
    $this->getAllTasks($this->path);
    $this->setStatus('in_progress');

    $newTask = [
      'id' => uniqid(),
      'taskName' => $this->taskName,
      'priority' => $this->priority,
      'status' => $this->status
    ];

    $this->tasks[] = $newTask;
  }

  public function deletetask(string $taskId)
  {
    $this->checkTaskById($this->tasks, $taskId);

    $filteredTasks = array_filter($this->tasks, function ($task) use ($taskId) {
      return $task['id'] !== $taskId;
    });

    $this->tasks = $filteredTasks;
  }

  public function getTasks(): array
  {
    $tasks = $this->tasks;

    uasort($tasks, function ($a, $b) {
      return $a['priority'] < $b['priority'];
    });

    return $tasks;
  }

  public function completeTask(string $taskId): void
  {
    $this->checkTaskById($this->tasks, $taskId);
    $this->setStatus('done');
    $tasks = $this->tasks;

    foreach ($tasks as &$task) {
      if ($task['id'] === $taskId) {
        $task['status'] = $this->status;
        break;
      }
    }

    $this->tasks = $tasks;
  }

  public function __destruct()
  {
    $this->saveTasks($this->tasks, $this->path);
  }
}