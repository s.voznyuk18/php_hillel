<?php
define('APP_DIR', __DIR__ . '/');
define('CLASSES_DIR', APP_DIR . 'classes/');
define('ENUMS_DIR', APP_DIR . 'enums/');
define('DB_DIR', APP_DIR . 'db/');