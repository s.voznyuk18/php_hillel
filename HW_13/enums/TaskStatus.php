<?php

enum TaskStatus: string
{
  case DONE = 'done';
  case IN_PROGRESS = 'in_progress';

  public static function values(): array
  {
    $cases = self::cases();
    return array_column($cases, 'value');
  }
}
