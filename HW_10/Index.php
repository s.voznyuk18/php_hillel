<?php
declare(strict_types=1);
function fibonacciGenerator(int $iterations = 10): Generator
{
  $firstNum = 0;
  $secondNum = 1;
  $counter = 0;

  while ($counter < $iterations) {
    yield $firstNum;
    $nexNum = $secondNum + $firstNum;
    $firstNum = $secondNum;
    $secondNum = $nexNum;
    $counter++;
  }
}

$numbers = fibonacciGenerator(10);

foreach ($numbers as $number) {
  echo $number . PHP_EOL;
}
