<?php

abstract class Employee
{
  protected string $position;
  public static float $basicSalary = 6000.00;

  protected float $coefficient;

  public function __construct(string $position)
  {
    $this->setPosition($position);
  }

  public static function getBasicSalary(): string
  {
    return self::$basicSalary;
  }

  public function setPosition(string $position): void
  {
    $position = trim(($position));
    if (strlen($position) < 3) {
      throw new Exception('must be mor 3');
    }
    $this->position = $position;
  }

  public function getPosition(): string
  {
    return $this->position;
  }

  public abstract function setCoefficient(float $coefficient): void;
  public abstract function calculateSalary(): float;
}