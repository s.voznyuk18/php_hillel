<?php

class Counter extends  Employee
{
  public function setCoefficient (float $coefficient):  void
  {
    if($coefficient <= 0) {
      throw new Exception('coefficient mus be more 0');
    }
    $this->coefficient = $coefficient;
  }

  public function calculateSalary (): float
  {
    return self::$basicSalary * $this->coefficient;
  }
}