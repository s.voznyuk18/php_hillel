<?php
require_once __DIR__ . '/constants.php';
require_once CLASSES_DIR . '/Employee.php';
require_once CLASSES_DIR .'/Counter.php';

try{
  $employee = new Counter('counter');
  $employee->setCoefficient(1.2);
  echo $employee->calculateSalary();
} catch(Exception $exception) {
  echo  $exception->getMessage() . PHP_EOL;
}