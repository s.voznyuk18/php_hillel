<?php

var_dump('10' == 10); //true
var_dump('10' === 10); //false

var_dump('10'== true); //true
var_dump('10' === true);  //false

var_dump('10' == 10.0); //true
var_dump('10'=== 10.0); //false
var_dump('10'== '10.0'); //true

var_dump('10'== null); //false
var_dump(''== null); // true
var_dump(''=== null); // false


var_dump('10' == intval('10')); //true
var_dump('10' === intval('10')); //false
var_dump(intval('10') === intval('10')); //true
var_dump(10 === intval('10')); //true
var_dump(boolval('10') === boolval('10')); //true
var_dump(boolval('') == boolval('10')); //false
var_dump(boolval('') === boolval(0)); //true
var_dump(10.2 === floatval('10.2')); //true
var_dump('10.2' === floatval('10.2')); //false
var_dump(null == floatval('')); //true
var_dump(null === floatval('')); //false











