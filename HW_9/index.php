<?php

declare(strict_types=1);

// #1
$i = 1;

while($i <= 10) {
  echo $i . PHP_EOL;
  $i++;
};

// #2
$f = 1;
$factorial = 1;

while($f <= 5) {
  $factorial *= $f;
  $f++;
}

echo $factorial;

// #3
$n = 1;

while($n <= 20) {
  if($n %2 === 0) echo $n . PHP_EOL;
  $n++;
}
