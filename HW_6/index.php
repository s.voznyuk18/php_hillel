<?php

declare(strict_types=1);

// function calcAreaCircle(int|float $r): int|float
// {
//   return pi() * $r ** 2;
// }

// $areaCicle = calcAreaCircle(10);
// echo $areaCicle . PHP_EOL;


// function calcAreaCircle(int|float &$areaCicle, int|float $r): void 
// {
//   $areaCicle = pi() * $r ** 2;
// }

// $areaCicle = 0;
// calcAreaCircle($areaCicle, 10);
// echo $areaCicle . PHP_EOL;


// function powner(int|float $num, int|float $pow): int|float
// {
//   return $num ** $pow;
// }

// $result = powner(2, 2);
// echo $result . PHP_EOL;


// function powner(int|float &$num, int|float $pow): void 
// {
//   $num **= $pow;
// }

// $number = 2;
// powner($number, 3);
// echo $number . PHP_EOL;