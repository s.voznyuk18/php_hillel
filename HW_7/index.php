<?php

declare(strict_types=1);

function multiplication(int $a, int $b, ?closure $callBack = null) : int
{
  $result  = $a * $b;

  if(isset($callBack)) {
    $callBack($result);
  }

  return $result;
}

multiplication(2, 3, function(int $result): void {
  echo $result . PHP_EOL;
});

$res = multiplication(2, 3);
echo $res . PHP_EOL;