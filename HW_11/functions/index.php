<?php

function recordLog (string $message, ?string $type  = 'action', ?string $fileName = LOG_FILE): void
{
  $pathToLog = LOGS_DIR . $fileName;
  $date = date('d-m-Y H:i:s');
  $message = "[$date][$type][$message]\n";

  $file = fopen($pathToLog, 'a+');
  fwrite($file, $message);
  fclose($file);
}

function readLogs(?string $fileName = LOG_FILE) :Generator
{
  $pathToLog = LOGS_DIR . $fileName;
  $file = fopen($pathToLog, 'r');

  while(($line = fgets($file)) !== false) {
    yield $line;
  }

  fclose($file);
}

