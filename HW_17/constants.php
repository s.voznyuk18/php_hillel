<?php
define('APP_DIR', __DIR__ . '/');
define('CLASSES_DIR', APP_DIR . 'classes');
define('TRAITS_DIR', APP_DIR . 'traits');
define('ENUMS_DIR', APP_DIR . 'enums');