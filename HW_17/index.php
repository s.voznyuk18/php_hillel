<?php
require_once __DIR__ . '/constants.php';
require_once ENUMS_DIR . '/ValidationRules.php';
require_once TRAITS_DIR . '/Validator.php';
require_once CLASSES_DIR . '/AuthController.php';
require_once CLASSES_DIR . '/MessageController.php';

$authData = [
  'email' => 'serhhgm',
  'password' => 'qwerty12345',
];
$authRules = [
  'email' => 'required|email|min:10',
  'password' => 'required|min:250',
];

$authConroller = new AuthController($authData, $authRules);
$authConroller->showErrors();

$messageData = [
  'title' => 'some title',
  'message' => 'qwerty12345',
];
$messageRules = [
  'title' => 'required|min:10',
  'message' => 'required|min:8',
];

$messagController = new MessageController($messageData, $messageRules);
$messagController->showErrors();