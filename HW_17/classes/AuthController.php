<?php

class AuthController
{
  use Validator;

  public function __construct(array $data, array $rules)
  {
    $this->validateData($data, $rules);
  }

  public function validateData(array $data, array $rules): void
  {
    try {
      $result = $this->validate($data, $rules);
      if ($result) {
        echo 'Validation OK' . PHP_EOL;
      } else {
        echo 'Validation Error' . PHP_EOL;
      }
    } catch (Exception $exception) {
      echo($exception->getMessage());
    }
  }

  public function showErrors(): void
  {
    var_dump($this->getErrors());
  }
}