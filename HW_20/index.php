<?php

require_once __DIR__ . '/constants.php';
require_once DB_CONFIG;
require_once CLASSES_DIR . '/Connector.php';
require_once CLASSES_DIR . '/SQLQueryBuilder.php';
require_once CLASSES_DIR . '/MysqlQueryBuilder.php';
require_once CLASSES_DIR . '/Repository.php';
require_once CLASSES_DIR . '/UsersRepository.php';
require_once CLASSES_DIR . '/OrdersRepository.php';
require_once CLASSES_DIR . '/ProductsRepository.php';
require_once CLASSES_DIR . '/OrderDetailRepository.php';

$connector = Connector::getInstance();
$sqlBuilder = new MysqlQueryBuilder();

$values = [
  'first_name' => 'SerhiiTest',
  'second_name' => 'Vozniuk',
  'email' => 'serhiiTest2225554@gmail.com',
  'password' => 'qwerty',
  'age' => 1,
  'gender' => 'male',
];

$fields = ['first_name', 'second_name'];

try {
  $usersRepository = new UsersRepository($connector, $sqlBuilder);
  $productsRep = new ProductsRepository($connector, $sqlBuilder);
  $result = $usersRepository->create($values);
//  $result = $usersRepository->find($fields);
//  $result = $usersRepository->findByParams($fields , 'email', 'serhiiTest2225554@gmail.com', '=');
//  $result = $productsRep->find('*');
  var_dump($result);
} catch (Exception $e) {
  echo $e . PHP_EOL;
}
