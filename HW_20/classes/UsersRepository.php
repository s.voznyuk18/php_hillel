<?php

class UsersRepository extends Repository
{
  protected static string $table = 'users';

  protected static string $primaryKey = 'id';
}