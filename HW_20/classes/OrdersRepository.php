<?php

class OrdersRepository extends Repository
{
  protected static string $table = 'orders';

  protected static string $primaryKey = 'order_id';
}