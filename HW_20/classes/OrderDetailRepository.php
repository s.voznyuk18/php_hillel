<?php

class OrderDetailRepository extends Repository
{
  protected static string $table = 'order_detail';

  protected static string $primaryKey = 'order_detail_id';
}