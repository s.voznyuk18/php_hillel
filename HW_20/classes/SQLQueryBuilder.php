<?php

interface SQLQueryBuilder
{
  public function select(string $table, array $fields): SQLQueryBuilder;

  public function where(string $field, string $value, string $operator = '='): SQLQueryBuilder;

  public function insert(string $table, array $values): SQLQueryBuilder;

  public function getSQL(): string;

  public function getValues(): array;
}