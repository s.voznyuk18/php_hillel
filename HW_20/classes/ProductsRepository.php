<?php

class ProductsRepository extends Repository
{
  protected static string $table = 'products';

  protected static string $primaryKey = 'product_id';
}