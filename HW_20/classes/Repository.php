<?php

class Repository
{
  protected static string $table = '';

  protected static string $primaryKey = 'id';

  private PDO $connector;

  private SQLQueryBuilder $buildger;

  public function __construct(PDO $connector, SQLQueryBuilder $buildger)
  {
    $this->connector = $connector;
    $this->buildger = $buildger;
  }

  public function create(array $value): bool|object
  {
    if (empty(static::$table)) {
      throw new Exception('Table is empty');
    }

    $query = $this->buildger->insert(static::$table, $value)->getSQL();
    $stmt = $this->connector->prepare($query);
    $stmt->execute($this->buildger->getValues());

    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public function find(array|string $fields): bool|object
  {
    if (empty(static::$table)) {
      throw new Exception('Table is empty');
    }

    $query = $this->buildger->select(static::$table, $fields)->getSQL();
    $stmt = $this->connector->prepare($query);
    $stmt->execute($this->buildger->getValues());

    return $stmt->fetch(PDO::FETCH_OBJ);
  }

  public function findByParams(
    array|string $fields,
    string       $field,
    string       $value,
    string       $operator
  ): bool|object {
    if (empty(static::$table)) {
      throw new Exception('Table is empty');
    }

    $query = $this->buildger->select(static::$table, $fields)->where($field, $value, $operator)->getSQL();
    $stmt = $this->connector->prepare($query);
    $stmt->execute($this->buildger->getValues());

    return $stmt->fetch(PDO::FETCH_OBJ);
  }
}