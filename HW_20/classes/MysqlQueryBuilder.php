<?php

class MysqlQueryBuilder implements SQLQueryBuilder
{
  private stdClass $query;

  private function reset(): void
  {
    $this->query = new stdClass();
  }

  public function insert(string $table, array $values): SQLQueryBuilder
  {
    if (empty($values)) {
      throw new Exception('values not empty');
    }

    $fields = implode(', ', array_keys($values));
    $placeholdersStr = implode(', ', array_map(fn($key) => ':' . $key, array_keys($values)));

    $this->reset();
    $this->query->base = "INSERT INTO " . $table . "($fields)" . " VALUES" . "($placeholdersStr)";
    $this->query->type = 'insert to';
    $this->query->values = $values;

    return $this;
  }
  public function select(string $table, array|string $fields = '*'): SQLQueryBuilder
  {
    $this->reset();
    $fields = $fields === '*' ? '*' : implode(', ', $fields);
    $this->query->base = "SELECT " . $fields . " FROM " . $table;
    $this->query->type = 'select';

    return $this;
  }

  public function where(string $field, string $value, string $operator = '='): SQLQueryBuilder
  {
    if (!in_array($this->query->type, ['select', 'update', 'delete'])) {
      throw new \Exception("WHERE can only be added to SELECT, UPDATE OR DELETE");
    }
    $this->query->where[] = "$field $operator :$field";
    $this->query->values[$field] = $value;

    return $this;
  }

  public function getSQL(): string
  {
    $query = $this->query;
    $sql = $query->base;
    if (!empty($query->where)) {
      $sql .= " WHERE " . implode(' AND ', $query->where);
    }
    return $sql;
  }

  public function getValues(): array
  {
    if (!isset($this->query->values)) {
      return [];
    }
    return $this->query->values;
  }
}