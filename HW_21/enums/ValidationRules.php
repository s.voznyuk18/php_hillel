<?php

enum ValidationRules: string
{
  case REQUIRED = 'required';
  case MIN = 'min';
  case EMAIL = 'email';

  public static function value(): array
  {
    $cases = self::cases();
    return array_column($cases, 'value');
  }
}