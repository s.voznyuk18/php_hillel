<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>


<div class="container">
    <form method="post" action="/calc">
        <div class="mb-3">
            <label for="firstNumber" class="form-label">First number</label>
            <input type="number" required name="firstNumber" class="form-control" id="firstNumber" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="secondNumber" class="form-label">second number</label>
            <input type="number" required class="form-control" id="secondNumber" name="secondNumber">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>
