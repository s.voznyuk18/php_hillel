<?php

function view(string $path, ?array $variables = []): void
{
  try {
    $view = new View();
    $view->render($path, $variables);
  } catch (Exception $exception) {
    var_dump($exception->getMessage());
  }
}