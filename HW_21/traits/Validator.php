<?php

trait Validator
{
  private array $data;
  private array $rules;
  private array $errors = [];

  private function validate(array $data, array $rules): bool
  {
    $this->parseValidationRules($rules);

    foreach ($this->rules as $fieldName => $fieldRules) {
      if (isset($data[$fieldName])) {
        foreach ($fieldRules as $ruleName => $ruleValue) {
          match ($ruleName) {
            ValidationRules::REQUIRED->value => $this->checkRequired($fieldName, $data[$fieldName]),
            ValidationRules::EMAIL->value => $this->checkEmail($fieldName, $data[$fieldName]),
            ValidationRules::MIN->value => $this->checkMinLength($fieldName, $ruleValue, $data[$fieldName]),
          };
        }
      }
    }
    return empty($this->errors);
  }

  private function parseValidationRules(array $rules): void
  {
    $rulesAssoc = [];

    foreach ($rules as $fieldName => $ruleString) {
      $ruleParts = explode('|', $ruleString);
      foreach ($ruleParts as $rule) {
        if (str_contains($rule, ':')) {
          [$key, $value] = explode(':', $rule);
          $this->validateRule($key);
          $rulesAssoc[$fieldName][$key] = $value;
        } else {
          $this->validateRule($rule);
          $rulesAssoc[$fieldName][$rule] = $rule;
        }
      }
    }
    $this->rules = $rulesAssoc;
  }

  private function validateRule(string $rule): bool
  {
    if (!in_array($rule, ValidationRules::value())) {
      throw new Exception('undefined rule');
    }
    return true;
  }

  private function checkRequired(string $fieldName, string $fieldValue): void
  {
    $fieldValue = trim($fieldValue);
    if (empty($fieldValue)) {
      $format = '%s is required/';
      $message = sprintf($format, $fieldName);
      $this->recordError($fieldName, $message);
    }
  }

  private function checkEmail(string $fieldName, string $fieldValue): void
  {
    $isEmail = filter_var($fieldValue, FILTER_VALIDATE_EMAIL);
    if (!$isEmail) {
      $format = '%s is not Email/';
      $message = sprintf($format, $fieldName);
      $this->recordError($fieldName, $message);
    }
  }

  private function checkMinLength(string $fieldName, int $ruleValue, string $fieldValue): void
  {
    if (strlen($fieldValue) < $ruleValue) {
      $format = '%s must be more than %d characters/';
      $message = sprintf($format, $fieldName, $ruleValue);
      $this->recordError($fieldName, $message);
    }
  }

  private function recordError(string $fieldName, string $message): void
  {
      $this->errors[$fieldName] .= $message;
  }

  public function getErrors(): array
  {
    return $this->errors;
  }
}