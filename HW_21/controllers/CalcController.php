<?php

class CalcController
{
  use Validator;

  public function showForm(): void
  {
    view('form.php');
  }

  public function showResult(): void
  {
    $this->showForm();
    $data = $_POST;
    $rules = [
      'firstNumber' => 'required',
      'secondNumber' => 'required',
    ];

    $isValidate = $this->validate($data, $rules);

    if ($isValidate) {
      extract($data);
      $result = $firstNumber + $secondNumber;
      view('showResult.php', ['result' => $result]);
    }
  }
}