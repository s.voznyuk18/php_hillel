<?php

class GreetingController
{
  public function greeting(): void
  {
    view('greeting.php');
  }
}