<?php
require_once __DIR__ . '/constants.php';
require_once CLASESS_DIR . 'BankAccount.php';

try {
  $bancAccount = new BankAccount();
  $bancAccount->setAccountNumber('123avbg');
  echo $bancAccount->getAccountNumber() .PHP_EOL;
  echo $bancAccount->depositFunds(100) .PHP_EOL;
  echo $bancAccount->depositFunds(100) .PHP_EOL;
  echo $bancAccount->getBalance() .PHP_EOL;
  echo $bancAccount->withdraw(5) .PHP_EOL;
  echo $bancAccount->getBalance() .PHP_EOL;
} catch (Exception $exception) {
  echo $exception . PHP_EOL;
}
