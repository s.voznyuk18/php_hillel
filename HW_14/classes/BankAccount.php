<?php

class BankAccount
{
  private string $accountNumber;
  private float $balance = 0;

  public function setAccountNumber(string $accountNumber): void
  {
    $accountNumber = trim($accountNumber);
    if (strlen($accountNumber) < 5) {
      throw new Exception('Account number should be at least 5 characters long');
    }
    $this->accountNumber = $accountNumber;
  }

  public function getAccountNumber(): string
  {
    return "Your account number is $this->accountNumber";
  }

  public function getBalance(): string
  {
    return "On account #$this->accountNumber: $this->balance $";
  }

  public function depositFunds(float $deposit): string
  {
    if ($deposit <= 0) {
      throw new  Exception('Deposit amount should be greater than 0');
    }
    $this->balance += $deposit;
    return "You have deposited $deposit $";
  }

  public function withdraw(float $amount): string
  {
    if ($amount <= 0) {
      throw new Exception('Withdrawal amount should be greater than 0');
    }
    if ($amount > $this->balance) {
      throw new Exception('Insufficient funds');
    }
    $this->balance -= $amount;
    return "You have withdrawn $amount $";
  }
}